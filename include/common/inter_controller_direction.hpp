#pragma once

#include <ros/ros.h>

#include <string>

//! \brief Base class for machines present in the assembly cell
//!
//! \tparam StateMsgT The type of the message containing the machine state
//! \tparam InputMsgT The type of the message containing the machine input signals
//! \tparam CommandMsgT The type of the message containing the machine commands
//! \tparam OutputMsgT The type of the message containing the machine output signals
template <typename Input1MsgT, typename InputMsgT,
          typename OutputMsgT>
class InterControllerDirection {
public:
    //! \brief Construct a new Machine Controller given a node handle and a topic
    //!
    //! \param node The node controlling the machine
    //! \param topic The topic associated with the machine
    InterControllerDirection(ros::NodeHandle node, const std::string& topic)
        : node_(node) {

        output_publisher_ = node.advertise<OutputMsgT>(topic + "/output", 10);

        input1_subscriber_ = node.subscribe( "robot_assembly_memory/output", 10, &InterControllerDirection::input1Callback, this);

        input_subscriber_ = node.subscribe(
            topic + "/input", 10, &InterControllerDirection::inputCallback, this);
    }

    //! \brief Where the handling of the machine's i/o must be performed
    virtual void process() = 0;

protected:
    //! \brief Send the output signals
    //!
    //! \param output_message The message to send
    void sendOuputs(OutputMsgT output_message) {
        output_publisher_.publish(output_message);
    }

       //! \brief Get the last received input1 signals
    //!
    //! \return Input1MsgT& Message with the input1 signals
    Input1MsgT& getInputs1() {
        return input1_message_;
    }

    //! \brief Get the last received input signals
    //!
    //! \return InputMsgT& Message with the input signals
    InputMsgT& getInputs() {
        return input_message_;
    }

private:
        //! \brief Called when new input signals are available
    //!
    //! \param message The new input signals
    void input1Callback(const typename Input1MsgT::ConstPtr& message) {
        input1_message_ = *message;
    }


    //! \brief Called when new input signals are available
    //!
    //! \param message The new input signals
    void inputCallback(const typename InputMsgT::ConstPtr& message) {
        input_message_ = *message;
    }

    ros::NodeHandle node_;
    ros::Publisher output_publisher_;
    ros::Subscriber input1_subscriber_;
    ros::Subscriber input_subscriber_;

    Input1MsgT input1_message_;
    InputMsgT input_message_;
};

