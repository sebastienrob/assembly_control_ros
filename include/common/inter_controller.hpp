#pragma once

#include <ros/ros.h>

#include <string>

//! \brief Base class for machines present in the assembly cell
//!
//! \tparam InputMsgT The type of the message containing the machine input signals
//! \tparam OutputMsgT The type of the message containing the machine output signals
template <typename InputMsgT,
          typename OutputMsgT>
class InterController {
public:
    //! \brief Construct a new Machine Controller given a node handle and a topic
    //!
    //! \param node The node controlling the machine
    //! \param topic The topic associated with the machine
    InterController(ros::NodeHandle node, const std::string& topic)
        : node_(node) {

        output_publisher_ = node.advertise<OutputMsgT>(topic + "/output", 10);

        input_subscriber_ = node.subscribe(
            topic + "/input", 10, &InterController::inputCallback, this);
    }

    //! \brief Where the handling of the machine's i/o must be performed
    virtual void process() = 0;

protected:
    //! \brief Send the output signals
    //!
    //! \param output_message The message to send
    void sendOuputs(OutputMsgT output_message) {
        output_publisher_.publish(output_message);
    }

    //! \brief Get the last received input signals
    //!
    //! \return InputMsgT& Message with the input signals
    InputMsgT& getInputs() {
        return input_message_;
    }

private:
    //! \brief Called when new input signals are available
    //!
    //! \param message The new input signals
    void inputCallback(const typename InputMsgT::ConstPtr& message) {
        input_message_ = *message;
    }

    ros::NodeHandle node_;
    ros::Publisher output_publisher_;
    ros::Subscriber input_subscriber_;

    InputMsgT input_message_;
};

