#pragma once

#include <ros/ros.h>
#include <string>

template <typename InputMsgT_supply_conveyor,
          typename OutputMsgT_supply_conveyor,
	  typename InputMsgT_camera,
	  typename OutputMsgT_camera,
	  typename InputMsgT_robot,
          typename OutputMsgT_robot,
	  typename InputMsgT_robot_control_direction,
          typename OutputMsgT_robot_control_direction,
	  typename InputMsgT_evacuation_conveyor,
          typename OutputMsgT_evacuation_conveyor,
          typename InputMsgT_assembly_station,
          typename OutputMsgT_assembly_station,
	  typename InputMsgT_robot_assembly_memory>

class InterControllerMaster {
public:
    InterControllerMaster(ros::NodeHandle node, const std::string& topic)
        : node_(node) {
//on publie sur le topic d'entrée i d'une node
//on souscrit au topic o d'une node
        output_publisher_supply_conveyor = node.advertise<InputMsgT_supply_conveyor>("supply_conveyor/input", 10);
        input_subscriber_supply_conveyor = node.subscribe("supply_conveyor/output", 10, &InterControllerMaster::input1Callback, this);

	output_publisher_camera = node.advertise<InputMsgT_camera>("camera/input", 10);
        input_subscriber_camera = node.subscribe("camera/output", 10, &InterControllerMaster::input2Callback, this);

	output_publisher_robot = node.advertise<InputMsgT_robot>("robot/input", 10);
        input_subscriber_robot = node.subscribe("robot/output", 10, &InterControllerMaster::input3Callback, this);

	output_publisher_robot_control_direction = node.advertise<InputMsgT_robot_control_direction>("robot_control_direction/input", 10);
        input_subscriber_robot_control_direction = node.subscribe("robot_control_direction/output", 10, &InterControllerMaster::input4Callback, this);

	output_publisher_evacuation_conveyor = node.advertise<InputMsgT_evacuation_conveyor>("evacuation_conveyor/input", 10);
        input_subscriber_evacuation_conveyor = node.subscribe("evacuation_conveyor/output", 10, &InterControllerMaster::input5Callback, this);

	output_publisher_assembly_station = node.advertise<InputMsgT_assembly_station>("assembly_station/input", 10);
        input_subscriber_assembly_station = node.subscribe("assembly_station/output", 10, &InterControllerMaster::input6Callback, this);

        output_publisher_robot_assembly_memory = node.advertise<InputMsgT_robot_assembly_memory>("robot_assembly_memory/input", 10);

    }

    //! \brief Where the handling of the machine's i/o must be performed
    virtual void process() = 0;

protected:
    //on envoie un message out(Master) de type in(node correspondante)
    void sendOuputs1(InputMsgT_supply_conveyor output_message) {
        output_publisher_supply_conveyor.publish(output_message);
    }
    void sendOuputs2(InputMsgT_camera output_message) {
    output_publisher_camera.publish(output_message);
    }
    void sendOuputs3(InputMsgT_robot output_message) {
    output_publisher_robot.publish(output_message);
    }
    void sendOuputs4(InputMsgT_robot_control_direction output_message) {
    output_publisher_robot_control_direction.publish(output_message);
    }
    void sendOuputs5(InputMsgT_evacuation_conveyor output_message) {
    output_publisher_evacuation_conveyor.publish(output_message);
    }
    void sendOuputs6(InputMsgT_assembly_station output_message) {
    output_publisher_assembly_station.publish(output_message);
    }
    void sendOuputs7(InputMsgT_robot_assembly_memory output_message) {
    output_publisher_robot_assembly_memory.publish(output_message);
    }


    //On récupère un message in(master) de type out(node) 
    OutputMsgT_supply_conveyor& getInputs1() {
        return input_message_supply_conveyor;
    }
    OutputMsgT_camera& getInputs2() {
        return input_message_camera;
    }
    OutputMsgT_robot& getInputs3() {
        return input_message_robot;
    }
    OutputMsgT_robot_control_direction& getInputs4() {
        return input_message_robot_control_direction;
    }
    OutputMsgT_evacuation_conveyor& getInputs5() {
        return input_message_evacuation_conveyor;
    }
    OutputMsgT_assembly_station& getInputs6() {
        return input_message_assembly_station;
    }

private:
    //on récupère un message in(master) de type out(node correspondante)
    void input1Callback(const typename OutputMsgT_supply_conveyor::ConstPtr& message) {
        input_message_supply_conveyor = *message;
    }
    void input2Callback(const typename OutputMsgT_camera::ConstPtr& message) {
        input_message_camera = *message;
    }
    void input3Callback(const typename OutputMsgT_robot::ConstPtr& message) {
        input_message_robot = *message;
    }
    void input4Callback(const typename OutputMsgT_robot_control_direction::ConstPtr& message) {
        input_message_robot_control_direction = *message;
    }
    void input5Callback(const typename OutputMsgT_evacuation_conveyor::ConstPtr& message) {
        input_message_evacuation_conveyor = *message;
    }
    void input6Callback(const typename OutputMsgT_assembly_station::ConstPtr& message) {
        input_message_assembly_station = *message;
    }
 
    ros::NodeHandle node_;

    ros::Publisher output_publisher_supply_conveyor;
    ros::Subscriber input_subscriber_supply_conveyor;

    ros::Publisher output_publisher_camera;
    ros::Subscriber input_subscriber_camera;

    ros::Publisher output_publisher_robot;
    ros::Subscriber input_subscriber_robot;

    ros::Publisher output_publisher_robot_control_direction;
    ros::Subscriber input_subscriber_robot_control_direction;

    ros::Publisher output_publisher_evacuation_conveyor;
    ros::Subscriber input_subscriber_evacuation_conveyor;

    ros::Publisher output_publisher_assembly_station;
    ros::Subscriber input_subscriber_assembly_station;

    ros::Publisher output_publisher_robot_assembly_memory;
    
    //on créee les in correpondante de type de message out correspondant
    OutputMsgT_supply_conveyor input_message_supply_conveyor;
    OutputMsgT_camera input_message_camera;
    OutputMsgT_robot input_message_robot;
    OutputMsgT_robot_control_direction input_message_robot_control_direction;
    OutputMsgT_evacuation_conveyor input_message_evacuation_conveyor;
    OutputMsgT_assembly_station input_message_assembly_station;
};


