#include <ros/ros.h>

#include <assembly_control_ros/camera_state.h>
#include <assembly_control_ros/camera_command.h>
#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>

#include <common/machine_controller.hpp>

class Camera : public MachineController<assembly_control_ros::camera_state,
                                        assembly_control_ros::camera_input,
                                        assembly_control_ros::camera_command,
                                        assembly_control_ros::camera_output> {
public:
    Camera(ros::NodeHandle node)
        : MachineController(node, "camera"), state_(State::Wait) {
    }

    virtual void process() override {
        assembly_control_ros::camera_command commands;
        assembly_control_ros::camera_output outputs;

        auto& inputs = getInputs();

	//go process camera by control of master
        switch (state_) {
        case State::Wait:
            if (inputs.GoProcessCameraMaster) {
                inputs.GoProcessCameraMaster = false;
                ROS_INFO("[Camera] Processing");
                state_ = State::Processing;
            }
            break;

        case State::Processing:
            commands.process = true;
            if (getState().done) {
                ROS_INFO("[Camera] Ready");
                state_ = State::Ready;
            }
            break;

        case State::Ready:
            switch (getState().part) {
            case 1:
                ROS_INFO("[Camera] Part 1");
                outputs.ProcessPart1 = true;
                outputs.ProcessPart2 = false;
                outputs.ProcessPart3 = false;
                break;
            case 2:
                ROS_INFO("[Camera] Part 2");
                outputs.ProcessPart1 = false;
                outputs.ProcessPart2 = true;
                outputs.ProcessPart3 = false;
                break;
            case 3:
                ROS_INFO("[Camera] Part 3");
                outputs.ProcessPart1 = false;
                outputs.ProcessPart2 = false;
                outputs.ProcessPart3 = true;
                break;
            default:
                ROS_INFO("[Camera] Unknown part");
                outputs.ProcessPart1 = false;
                outputs.ProcessPart2 = false;
                outputs.ProcessPart3 = false;
                break;
            }

            sendOuputs(outputs);
            ROS_INFO("[Camera] Wait");
            state_ = State::Wait;
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { Wait, Processing, Ready };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "camera");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Camera camera(node);

    while (ros::ok()) {
        camera.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
