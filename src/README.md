# Note/Template pour création node c++

```ruby
#include <ros/ros.h>

#include <assembly_control_ros/supply_conveyor_state.h>
#include <assembly_control_ros/supply_conveyor_command.h>
#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <common/machine_controller.hpp>
```

```
La classe template permet de définir un ensemble de classe et
qui va pouvoir être génerer automatiquement en fonction des
paramétres qu'ont lui passe (paramétres template).
Template fait les choses de manières statique (création à la compilation
et non lors de l'execution, dynamique).
DONC je crée une classe et je lui dit tu va travailler avec ces 4 types de 
mesages la.
Les paramètres, sont des types, c'est notre classe enfaite, ce sont des 
structures qui contiennent les données des messages.
Ainsi le compilateur va faire un copier/coller de la template(patron) et 
remplacer ces paramètres par ceux donner et les compiler pour ces types la.
Analogie avec std::vector<int> qui est un type complet, un type spécialisé, 
finalement est un template car généré lors de la compilation  (présence de <>).
Template permet donc de générer des familles de fonctions
```

```ruby
class SupplyConveyor
    : public MachineController<assembly_control_ros::supply_conveyor_state,
                               assembly_control_ros::supply_conveyor_input,
                               assembly_control_ros::supply_conveyor_command,
                               assembly_control_ros::supply_conveyor_output> {
```

```
Le constructeur qui prend nodehandle, je le passe à machinecontroler car c'est
lui qui va call subscribe et publish et j'initialise mon état interne.
```

```ruby
public:
    SupplyConveyor(ros::NodeHandle node)
        : MachineController(node, "supply_conveyor"), state_(State::On) {
    }
```

```
La fonction process pour implémenter le réseau de pétri lui-même et 
on l'implémente comme une machine à états finit.
Les deux première lignes auraient pu être mis au dessus mais
on aurais besoin de déclarer tout les états (plus fastidieux)
```

```ruby
virtual void process() override {
     assembly_control_ros::supply_conveyor_command commands;
     assembly_control_ros::supply_conveyor_output outputs;

auto& inputs = getInputs(); 

switch (state_) {
```

```
commands est intialiser à false et comme on ne modifie rien,
cela restera à faux.
pas de commands.off car ça reviens à l'initial à chaque fois, je
dit juste tient la j'ai besoin de mettre en route tel actionneur.
N.B: en bas c'est juste pour un auto remplacer des inputs (moins fastidieux).
```

```
Si on est dans cet état la, on dit que la commande du tapis, on, est
égal a true (on = contenu du message).
getState (methode implémenter dans MachineController nous fournit 
l'état courant (le contenu du message state qui à était émis par vrep
et on regarde optical_barrier, est ce que la barrière optique à était franchis).
Si oui je dit que dans mes places de sorties, je marque part_available(message)
et je le dit = à true.
Le sendOutputs pour lui dire j'ai marquer cette place, 
il faut l'envoyer vrep
```

```ruby
        case State::On:
            commands.on = true;
            if (getState().optical_barrier) {
                outputs.part_available = true;
                sendOuputs(outputs);

                ROS_INFO("[SupplyConveyor] Off");
                state_ = State::Off;
            }
            break;
```

```
La transition est validé si la place d'entrée in est marqué.
Pas besoin de commands.off ca ça réinitialise lors de la boucle, 
car dans ce petri information lié à qu'un seul état, donc juste
ici j'ai besoin de mettre en route tel actionneur.
On doit le mettre à false car ça ne ce fait pas automatiquement, 
du à la manière dont ros fonctionne.
Ensuite on quitte la fonction, et la prochaine itération on reviendra,
et l'état aura changer, on implémentera notre 2ème transition.
```

```ruby
        case State::Off:
            
            if (inputs.restart) {
            
                inputs.restart = false;

                ROS_INFO("[SupplyConveyor] On");
                state_ = State::On;
            }
            break;
        }
        sendCommands(commands);
    }
```

```
On declare une énumeration etat et on recueille une instance et on fait notre
switch sur l'état courant.
etats on/off pour le tapis (place petri label).
On implémente le process dans le main.
```

```ruby
private:

    //etats on/off pour le tapis (place petri label)
    enum class State { On, Off }; 
    
    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "supply_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    SupplyConveyor conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
```
