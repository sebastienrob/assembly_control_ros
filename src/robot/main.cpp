#include <ros/ros.h>

#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>

class Robot
    : public MachineController<assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {

//global variable for control position without bug				       
int Position = 0; //Assembly=0 ; Supply=1 ; Evacuation =2				       
bool ValidatePlaceLeft=false;
bool ValidatePlaceRight=false;
bool BackIdle1=false;
bool BackIdle2=false;

public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Idle) {
    }
   
    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();
	auto& states = getState();

  //state of position's robot
  switch(Position) {
  case 0:
          if (getState().at_supply_conveyor & ValidatePlaceRight) {
             Position=1;
             BackIdle2=true;
	     ValidatePlaceRight=false;
          }
        break;
  case 1 :
          if (getState().at_evacuation_conveyor & ValidatePlaceRight) {
             Position = 2;
             BackIdle2=true;
	     ValidatePlaceRight=false;
          }
          else if (getState().at_assembly_station & ValidatePlaceLeft) {
                  Position=0;
                  BackIdle1=true;
		  ValidatePlaceLeft=false;
          }
          break;
  case 2 :
          if (getState().at_supply_conveyor & ValidatePlaceLeft){
             Position =1;
             BackIdle1 = true;
	     ValidatePlaceLeft=false;
          }
          break;
  }


        //control of robot'action and robot'move
        switch (state_) {

        case State::Idle:
            if (inputs.GoLeft) {
		inputs.GoLeft = false;
		ValidatePlaceLeft=true;
	        ROS_INFO("[RobotMove] Left");	
                state_ = State::Left;
            }
	    else if (inputs.GoRight) {
		    inputs.GoRight = false;
		    ValidatePlaceRight=true;
		    ROS_INFO("[RobotMove] Right");
                    state_ = State::Right;
	    }
	    else if (inputs.GoGrasp) {
		    inputs.GoGrasp = false;
	            ROS_INFO("[RobotAction] Grasp");
                    state_ = State::Grasp;
            }
	    else if (inputs.GoRelease) {
                    inputs.GoRelease = false;
		    ROS_INFO("[RobotAction] Release");
                    state_ = State::Release;
	    }
	     else if (inputs.AssemblyPart1) {
                     inputs.AssemblyPart1 = false;
                     ROS_INFO("[RobotAction] Assemble Part 1");
                     state_ = State::Ass1;
            }
             else if (inputs.AssemblyPart2) {
                     inputs.AssemblyPart2 = false;
                     ROS_INFO("[RobotAction] Assemble Part 2");
                     state_ = State::Ass2;
            }
             else if (inputs.AssemblyPart3) {
                     inputs.AssemblyPart3 = false;
                     ROS_INFO("[RobotAction] Assemble Part 3");
                     state_ = State::Ass3;
            }

            break;

        case State::Left:
	     commands.move_left = true;
            if (BackIdle1) {   
		BackIdle1 = false;     
                outputs.StopLeft = true;
                sendOuputs(outputs);
		ROS_INFO("[RobotMove] Idle");
                state_ = State::Idle;
            }
            break;

	case State::Right:
	     commands.move_right = true;
            if (BackIdle2) {    
	        BackIdle2 = false;		
                outputs.StopRight = true;
		sendOuputs(outputs);
		ROS_INFO("[RobotMove] Idle");
                state_ = State::Idle;
            }
            break;

	    case State::Grasp:
	     commands.grasp = true;
            if (getState().part_grasped) {    
                outputs.DoneGrasp = true;
                sendOuputs(outputs);
		ROS_INFO("[RobotAction] idle");
                state_ = State::Idle;
            }
            break;

	case State::Release:
	     commands.release = true;
            if (getState().part_released) {
                outputs.DoneReleased = true;
		sendOuputs(outputs);
		ROS_INFO("[RobotAction] idle");
                state_ = State::Idle;
            }
            break;

	            case State::Ass1:
             commands.assemble_part1 = true;
            if (getState().part1_assembled && getState().part_grasped == false) {    
                outputs.DoneAssPart1 = true;
                sendOuputs(outputs);
		ROS_INFO("[RobotAction] idle");
                state_ = State::Idle;
            }
            break;

	            case State::Ass2:
             commands.assemble_part2 = true;
            if (getState().part2_assembled && getState().part_grasped == false) {
                outputs.DoneAssPart2 = true;
                sendOuputs(outputs);
		ROS_INFO("[RobotAction] idle");
                state_ = State::Idle;
		
            }
            break;

	            case State::Ass3:
             commands.assemble_part3 = true;
            if (getState().part3_assembled && getState().part_grasped == false) {    
                outputs.DoneAssPart3 = true;
                sendOuputs(outputs);
		ROS_INFO("[RobotAction] idle");
                state_ = State::Idle;
            }
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { Idle, Left, Right, Grasp, Release, Ass1, Ass2, Ass3 };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


