#include <ros/ros.h>

#include <assembly_control_ros/robot_control_direction_input.h>
#include <assembly_control_ros/robot_control_direction_output.h>
#include <assembly_control_ros/robot_assembly_memory_output.h>

#include <common/inter_controller_direction.hpp>

class RobotControlDirection
    : public InterControllerDirection<assembly_control_ros::robot_assembly_memory_output,
                                      assembly_control_ros::robot_control_direction_input,
                                      assembly_control_ros::robot_control_direction_output> {
public:
    RobotControlDirection(ros::NodeHandle node)
        : InterControllerDirection(node, "robot_control_direction") {
    }

    virtual void process() override {
        assembly_control_ros::robot_control_direction_output outputs;
       
        auto& inputs = getInputs();
	auto& inputs1 = getInputs1();

        //c'est la node robot_assembly_memory qui ce charge de reset les 
	//input de Emptyx/Fullx permettant de memorises si full/empty place.
	//Le master lui initialisera le GoNewProcess à true.
	
             //Robot direction for Part1 
             if (inputs1.Empty1 && inputs.ProcessPart1Master && inputs.GoNewProcess) {
		  inputs.ProcessPart1Master = false;
	          inputs.GoNewProcess = false;
		  outputs.Part1GoProcessMaster = true;
		  outputs.RobGoAssNow = true;
		  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] ASS1");
	     }
             else if (inputs1.Full1 && inputs.ProcessPart1Master && inputs.GoNewProcess) {
                  inputs.ProcessPart1Master = false;
                  inputs.GoNewProcess = false;
                  outputs.RobGoEvacNow = true;
                  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] EVAC1");   
	    }
                    
             //Robot direction for Part2     
	     else if (inputs1.Empty2 && inputs.ProcessPart2Master && inputs.GoNewProcess) {
                  inputs.ProcessPart2Master = false;
                  inputs.GoNewProcess = false;
                  outputs.Part2GoProcessMaster = true;
                  outputs.RobGoAssNow = true;
                  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] ASS2");
             }
             else if (inputs1.Full2 && inputs.ProcessPart2Master && inputs.GoNewProcess) {
                  inputs.ProcessPart2Master = false;
                  inputs.GoNewProcess = false;
                  outputs.RobGoEvacNow = true;
                  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] EVAC2");
            }

             //Robot direction for Part3
             else if (inputs1.Empty3 && inputs.ProcessPart3Master && inputs.GoNewProcess) {
                  inputs.ProcessPart3Master = false;
                  inputs.GoNewProcess = false;
                  outputs.Part3GoProcessMaster = true;
                  outputs.RobGoAssNow = true;
                  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] ASS3");
             }
             else if (inputs1.Full3 && inputs.ProcessPart3Master && inputs.GoNewProcess) {
                  inputs.ProcessPart3Master = false;
                  inputs.GoNewProcess = false;
                  outputs.RobGoEvacNow = true;
                  sendOuputs(outputs);
                  ROS_INFO("[RobotDirection] EVAC3");
	     }
            
}    
        
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot_control_direction");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    RobotControlDirection robotdirection(node);

    while (ros::ok()) {
        robotdirection.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
