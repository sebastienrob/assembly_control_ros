#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_output> {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::Wait) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();

	// if all part are inside case of assembly so go process evacuation and maj of memory
        switch (state_) {
        case State::Wait:
            if (inputs.Part1AssemblyEvac && inputs.Part2AssemblyEvac && inputs.Part3AssemblyEvac) {
		inputs.Part1AssemblyEvac = false;
		inputs.Part2AssemblyEvac = false;
		inputs.Part3AssemblyEvac = false;
                ROS_INFO("[AssemblyStation] Go Check for validate");
                state_ = State::Check;
            }
            break;

        case State::Check:
	    commands.check = true;
            if (getState().valid) {
                ROS_INFO("[AssemblyStation] Validate go Evacuated");
                state_ = State::Evacuate;
            }
            break;

	     case State::Evacuate:
            if (getState().evacuated) {
                outputs.AllPositionPartEmpty = true;
		sendOuputs(outputs);
                ROS_INFO("[AssemblyStation] Evacuation all parts");
                state_ = State::Wait;
            }
            break;

        }

        sendCommands(commands);
    }

private:
    enum class State { Wait, Check, Evacuate };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation assemblystation(node);

    while (ros::ok()) {
        assemblystation.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


