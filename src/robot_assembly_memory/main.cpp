#include <ros/ros.h>

#include <assembly_control_ros/robot_assembly_memory_input.h>
#include <assembly_control_ros/robot_assembly_memory_output.h>

#include <common/inter_controller.hpp>

//global for my memory controller
assembly_control_ros::robot_assembly_memory_output outputs;

class RobotAssemblyMemory
    : public InterController<assembly_control_ros::robot_assembly_memory_input,
                             assembly_control_ros::robot_assembly_memory_output> {
public:
    RobotAssemblyMemory(ros::NodeHandle node)
        : InterController(node, "robot_assembly_memory"), state_(State::Init) {
    }

    virtual void process() override {

        auto& inputs = getInputs();

	//process for save memory place in assembly_station
        switch (state_) {
        case State::Init:
		ros::Duration(5.0).sleep(); //get time for simulator process (bug : "Simulation started" begin after Init)
                outputs.Full1 = false;
                outputs.Empty1 = true;
                outputs.Full2 = false;
                outputs.Empty2 = true;
                outputs.Full3 = false;
                outputs.Empty3 = true;
                sendOuputs(outputs);
                ROS_INFO("[RobotMemory] Init");
                state_ = State::ReadyMemory;
            break;

        case State::Reset:
            if (inputs.AllPositionPartEmptyMaster) {
		inputs.AllPositionPartEmptyMaster = false;

		outputs.Full1 = false;
		outputs.Empty1 = true;
		outputs.Full2 = false;
                outputs.Empty2 = true;
		outputs.Full3 = false;
                outputs.Empty3 = true;
		sendOuputs(outputs);

	        ROS_INFO("[RobotMemory] All Empty");	
                state_ = State::ReadyMemory;
            }
            break;

        case State::ReadyMemory:
            if (inputs.GoMemoryFull1) {
		inputs.GoMemoryFull1 = false;    
                outputs.Full1 = true;
		outputs.Empty1 = false;
		ROS_INFO("[RobotMemory] memory full1");
                sendOuputs(outputs);
            }
	       else if (inputs.GoMemoryFull2) {
                       inputs.GoMemoryFull2 = false;
                       outputs.Full2 = true;
                       outputs.Empty2 = false;
		       ROS_INFO("[RobotMemory] memory full2");
                       sendOuputs(outputs);
               }
	          else if (inputs.GoMemoryFull3) {
                          inputs.GoMemoryFull3 = false;
                          outputs.Full3 = true;
                          outputs.Empty3 = false;
			  ROS_INFO("[RobotMemory] memory full3");
                          sendOuputs(outputs);
                  }
	             else if (outputs.Full1 && outputs.Full2 && outputs.Full3) { 
                    state_ =  State::Reset;
	             }
            break;

        }
    }

private:
    enum class State { Init, Reset, ReadyMemory };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot_assembly_memory");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    RobotAssemblyMemory robotassmemory(node);

    while (ros::ok()) {
        robotassmemory.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

