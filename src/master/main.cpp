#include <ros/ros.h>

#include <assembly_control_ros/robot_assembly_memory_input.h>
//output process is managed by memory node 

#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>

#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <assembly_control_ros/robot_control_direction_input.h>
#include <assembly_control_ros/robot_control_direction_output.h>

#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>


#include <common/inter_controller_master.hpp>

//global cause i need to save them and don't reset  
assembly_control_ros::robot_control_direction_input outputsDIR;
assembly_control_ros::assembly_station_input outputsASS;

class Master
    : public InterControllerMaster<assembly_control_ros::supply_conveyor_input,
                             assembly_control_ros::supply_conveyor_output,
	                     assembly_control_ros::camera_input,
                             assembly_control_ros::camera_output,
			     assembly_control_ros::robot_input,
                             assembly_control_ros::robot_output,
                             assembly_control_ros::robot_control_direction_input,
                             assembly_control_ros::robot_control_direction_output,
                             assembly_control_ros::evacuation_conveyor_input,
                             assembly_control_ros::evacuation_conveyor_output,
			     assembly_control_ros::assembly_station_input,
                             assembly_control_ros::assembly_station_output,
			     assembly_control_ros::robot_assembly_memory_input> {
public:
    Master(ros::NodeHandle node)
        : InterControllerMaster(node, "master"), state_(State::Init) {
    }

//i/o for master are o/i of others node (i-->o ; o-->i)

    virtual void process() override {	    
assembly_control_ros::evacuation_conveyor_input outputsEVAC;
assembly_control_ros::camera_input outputsCAM;
assembly_control_ros::robot_input outputsROB;
assembly_control_ros::supply_conveyor_input outputsSUP;
assembly_control_ros::robot_assembly_memory_input outputsMEM;

	    
auto& inputsSUP = getInputs1();
auto& inputsCAM = getInputs2();
auto& inputsROB = getInputs3();
auto& inputsDIR = getInputs4();
auto& inputsEVAC = getInputs5();
auto& inputsASS = getInputs6();

//PART 1 --> Independant Process Of Master 

        //supply-->camera
        if (inputsSUP.GoProcessCamera) {
           inputsSUP.GoProcessCamera = false;
           outputsCAM.GoProcessCameraMaster = true;
           sendOuputs2(outputsCAM);
           ROS_INFO("[Master] supply say go process to camera ");
        }

        //Camera-->Direction
        if (inputsCAM.ProcessPart1) {
           inputsCAM.ProcessPart1 = false;
           outputsDIR.ProcessPart1Master = true;
           ROS_INFO("[Master] cam reco Part1 for Direction ");
        }
        else if (inputsCAM.ProcessPart2) {
           inputsCAM.ProcessPart2 = false;
           outputsDIR.ProcessPart2Master = true;
           ROS_INFO("[Master] cam reco part2 for direction ");
        }
        else if (inputsCAM.ProcessPart3) {
           inputsCAM.ProcessPart3 = false;
           outputsDIR.ProcessPart3Master = true;
           ROS_INFO("[Master] cam reco part3 for direction ");
        }
        //assembly-->memory
        if (inputsASS.AllPositionPartEmpty) {
           inputsASS.AllPositionPartEmpty = false;
           outputsMEM.AllPositionPartEmptyMaster = true;
           sendOuputs7(outputsMEM);
           ROS_INFO("[Master] ass empty all for memory ");
        }

        //-->management of direction
        if (outputsDIR.GoNewProcess && (outputsDIR.ProcessPart1Master || outputsDIR.ProcessPart2Master || outputsDIR.ProcessPart3Master)) {
           sendOuputs4(outputsDIR);
           outputsDIR.ProcessPart1Master = false;
           outputsDIR.ProcessPart2Master = false;
           outputsDIR.ProcessPart3Master = false;
           outputsDIR.GoNewProcess = false;
           ROS_INFO("[Master] Direction Can be choice ");
        }

        //-->management of assembly
        if (outputsASS.Part1AssemblyEvac && outputsASS.Part2AssemblyEvac && outputsASS.Part3AssemblyEvac) {
           sendOuputs6(outputsASS);
           outputsASS.Part1AssemblyEvac=false;
           outputsASS.Part2AssemblyEvac=false;
           outputsASS.Part3AssemblyEvac=false;
        }


//PART 2 --> Main Branch Of Master (Goals : do parallel processes)

        switch (state_) {

	//Init1 : just for true of GoNewProcess	
        case State::Init:
		ros::Duration(3.0).sleep(); //get time for simulator process (bug : "Simulation started" begin after Init)
                outputsDIR.GoNewProcess = true;
                ROS_INFO("[Master] Init");
                state_ = State::Assemble_CommandRight;
            break;	

	//Main1 Branch : end of assembly part so go right    
        case State::Assemble_CommandRight:
		outputsROB.GoRight = true;
		sendOuputs3(outputsROB);
	        ROS_INFO("[Master] End AssemblyPart, Go Right");	
                state_ = State::CommandRight_AssOrEvac; 
            break;

	//Main2 Branch : right ok and we decide to evacuate or assembly the next part
	//in the same time, grasp part
	//stopright/stopleft's inpus condition are necessary for dual processes    
	case State::CommandRight_AssOrEvac:
            if ((inputsROB.StopRight || inputsROB.StopLeft) && inputsDIR.RobGoAssNow) {    
                inputsROB.StopRight = false;
		inputsDIR.RobGoAssNow = false;
                outputsROB.GoGrasp = true;
		sendOuputs3(outputsROB);
		ROS_INFO("[Master] Ass : FIRST  Go Grasp ");
		state_ = State::AssOrEvac_AssGraspLeft;
	    }

	    else if ((inputsROB.StopRight || inputsROB.StopLeft) && inputsDIR.RobGoEvacNow) {    
                inputsROB.StopRight = false;
                inputsDIR.RobGoEvacNow = false;
		inputsROB.StopLeft = false;
                outputsROB.GoGrasp = true;
		sendOuputs3(outputsROB);
		outputsEVAC.EvacuationMustStop = true;
                sendOuputs5(outputsEVAC);
                ROS_INFO("[Master] Evac : FIRST Go Grasp and Evacuation Stop ");
                state_ = State::AssOrEvac_EvacGraspRight;
            }
            break;

	//Ass1 : part grasped +  supplyconveyor can on and go right     
        case State::AssOrEvac_AssGraspLeft:
            if (inputsROB.DoneGrasp) {
                inputsROB.DoneGrasp = false;
                outputsSUP.SupplyCanOn = true;
		sendOuputs1(outputsSUP);
		outputsROB.GoLeft = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Ass : Grasp Ok and Supply On and Go Left ");
                state_ = State::AssGraspLeft_WhichPart;
            }
            break;

	//Ass2 : choice of part to assembly     
        case State::AssGraspLeft_WhichPart:
            if (inputsROB.StopLeft && inputsDIR.Part1GoProcessMaster) {
                inputsROB.StopLeft = false;
                inputsDIR.Part1GoProcessMaster = false;
                outputsROB.AssemblyPart1 = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Ass : Go Left Ok, Choice Part 1, Go Assemble");
                state_ = State::WhichPart_Assemble;
            }
	    else if (inputsROB.StopLeft && inputsDIR.Part2GoProcessMaster) {
		inputsROB.StopLeft = false;
                inputsDIR.Part2GoProcessMaster = false;
                outputsROB.AssemblyPart2 = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Ass : Go Left Ok, Choice Part 2, Go Assemble");
                state_ = State::WhichPart_Assemble;
	    }
	    else if (inputsROB.StopLeft && inputsDIR.Part3GoProcessMaster) {    
	        inputsROB.StopLeft = false;
                inputsDIR.Part3GoProcessMaster = false;
                outputsROB.AssemblyPart3 = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Ass : Go Left Ok, Choice Part 3, Go Assemble");
                state_ = State::WhichPart_Assemble;
	    }
            break;

	//Ass3 : we assemble and go back to state's init of supply    
        case State::WhichPart_Assemble:
             if (inputsROB.DoneAssPart1) {
                inputsROB.DoneAssPart1 = false;
                outputsASS.Part1AssemblyEvac = true;
		outputsMEM.GoMemoryFull1 = true;
		sendOuputs7(outputsMEM);
		outputsDIR.GoNewProcess = true;	
                ROS_INFO("[Master] Ass : Assembled Ok, fullPartMemory1 and NewProcess and Next1GoEvac ");
                state_ = State::Assemble_CommandRight;
            }
            else if (inputsROB.DoneAssPart2) {
                inputsROB.DoneAssPart2 = false;
                outputsASS.Part2AssemblyEvac = true;
		outputsMEM.GoMemoryFull2 = true;
		sendOuputs7(outputsMEM);
                outputsDIR.GoNewProcess = true;
                ROS_INFO("[Master] Ass : Assembled Ok, fullPartMemory2 and NewProcess and Next2GoEvac ");
                state_ = State::Assemble_CommandRight;
            }
            else if (inputsROB.DoneAssPart3) {
                inputsROB.DoneAssPart3 = false;
                outputsASS.Part3AssemblyEvac = true;
		outputsMEM.GoMemoryFull3 = true;
		sendOuputs7(outputsMEM);
                outputsDIR.GoNewProcess = true;
                ROS_INFO("[Master] Ass : Assembled Ok, fullPartMemory3 and NewProcess and Next3GoEvac ");
                state_ = State::Assemble_CommandRight;
            }
            break;

	//Evac1 : part grasped, supplyconveyor can on and go right
        case State::AssOrEvac_EvacGraspRight:
            if (inputsROB.DoneGrasp) {
                inputsROB.DoneGrasp = false;
                outputsSUP.SupplyCanOn = true;
		sendOuputs1(outputsSUP);
                outputsROB.GoRight = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Evac : Grasp Ok and Supply On and Go Right ");
                state_ = State::EvacGraspRight_EvacRelease;
            }
            break;

        //Evac2 : right ok and Evac okstop so go Release part
        case State::EvacGraspRight_EvacRelease:
            if (inputsEVAC.EvacuationIsStopped && inputsROB.StopRight) {
                inputsEVAC.EvacuationIsStopped = false;
		inputsROB.StopRight = false;
                outputsROB.GoRelease = true;
                sendOuputs3(outputsROB);
                ROS_INFO("[Master] Evac : Evac stopped and right stop, go release ");
                state_ = State::EvacRelease_EvacRunLeft;
            }
            break;

 	//Evac3 : part released, evacuationconveyor can on and go left
        case State::EvacRelease_EvacRunLeft:
            if (inputsROB.DoneReleased) {
                inputsROB.DoneReleased = false;
                outputsROB.GoLeft = true;
		sendOuputs3(outputsROB);
		outputsEVAC.EvacuationCanRun = true;
                sendOuputs5(outputsEVAC);
                ROS_INFO("[Master] Evac : Released, go left, Evac Run ");
                state_ = State::EvacRunLeft_EvacOkNewProcess;
            }
            break;

        //Evac4 : we have evacuated part, so go back choice between Assembly ou Evacuation cause already at supply
        case State::EvacRunLeft_EvacOkNewProcess:
            if (inputsROB.StopLeft) {
                // inputsROB.StopLeft = false; //reset this next state
                outputsDIR.GoNewProcess = true;
		//printf("stopright: %d, stopleft: %d, robgoevacnow :%d\n", inputsROB.StopRight, inputsROB.StopLeft, inputsDIR.RobGoEvacNow); //for correct my bug
                ROS_INFO("[Master] Evac : Ok left, back to AssOrEvac ");
                state_ = State::CommandRight_AssOrEvac ;
            }
            break;	    

        }


    }

private:
    enum class State {Init, Assemble_CommandRight, CommandRight_AssOrEvac, AssOrEvac_AssGraspLeft, AssGraspLeft_WhichPart, WhichPart_Assemble, AssOrEvac_EvacGraspRight, EvacGraspRight_EvacRelease, EvacRelease_EvacRunLeft, EvacRunLeft_EvacOkNewProcess };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "master");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Master robotmaster(node);

    while (ros::ok()) {
        robotmaster.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

